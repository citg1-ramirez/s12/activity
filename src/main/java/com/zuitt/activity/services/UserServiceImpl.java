package com.zuitt.activity.services;

import com.zuitt.activity.models.User;
import com.zuitt.activity.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService{


    @Autowired
    UserRepository userRepository;

    @Override
    public void createUser(User user) {
        userRepository.save(user);
    }

    @Override
    public Iterable getUsers() {
        return userRepository.findAll();
    }

    @Override
    public ResponseEntity updateUser(Long userid, User user) {
        User userUpdate = userRepository.findById(userid).get();

        userUpdate.setUsername(user.getUsername());
        userUpdate.setPassword(user.getPassword());

        userRepository.save(userUpdate);

        return new ResponseEntity("User updated successfully.", HttpStatus.OK);
    }

    @Override
    public ResponseEntity deleteUser(Long userid) {
        userRepository.deleteById(userid);
        return new ResponseEntity("User deleted successfully.", HttpStatus.OK);
    }
}

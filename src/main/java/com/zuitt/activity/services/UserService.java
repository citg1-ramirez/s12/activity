package com.zuitt.activity.services;

import com.zuitt.activity.models.User;
import org.springframework.http.ResponseEntity;

public interface UserService {

    void createUser(User user);

    Iterable getUsers();

    ResponseEntity updateUser (Long userid, User user);

    ResponseEntity deleteUser (Long userid);
}
